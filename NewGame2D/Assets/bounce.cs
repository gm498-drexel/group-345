﻿using UnityEngine;
using System.Collections;
public class bounce : MonoBehaviour
{
    public float Power;
    public float Angle;



    public Vector3 MoveSpeed;
    //private Vector3 GritySpeed = Vector3.zero;
    private float dTime;
    //private Vector3 currentAngle;
    // Use this for initialization
    public Rigidbody2D rig;
    public bool triger = false;
    public bool initial = false;

    void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        //MoveSpeed = Quaternion.Euler(new Vector3(0,0, Angle)) * Vector3.right * Power;
        //currentAngle = Vector2.zero;
    }
    void Update()
    {
        if (triger == true && initial == true)
        {
            /*
            Debug.Log(Power);
            GritySpeed.y = Gravity * (dTime += Time.fixedDeltaTime);
            rig.velocity = new Vector2((MoveSpeed.x + GritySpeed.x), (MoveSpeed.y + GritySpeed.y));
            currentAngle.z = Mathf.Atan((MoveSpeed.y + GritySpeed.y) / MoveSpeed.x) * Mathf.Rad2Deg;
            transform.eulerAngles = currentAngle;
            */
            rig.velocity = Vector3.zero;
            rig.angularVelocity = 0f;
            Vector3 dir = Quaternion.AngleAxis(Angle, Vector3.forward) * Vector3.right;
            rig.AddForce(dir * Power);
            // Debug.Log(dir);
            // Debug.Log(Angle);
            //Debug.Log(Power);
            triger = false;
        }
        /*
        //Debug.Log(IsGround());
        if (IsGround()) {
            Debug.Log("Im ground");
            rig.velocity = Vector3.zero;
            rig.angularVelocity = 0f;
        }
        */
        
    }
    /*
    private bool IsGround() {
        return Physics.Raycast(transform.position,Vector2.down,0.51f);
    }
    */
}
