﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class mv : MonoBehaviour {
    public LayerMask groundLayer;
    public float speed;
    public Rigidbody2D rig;
    public float Jump;
    RaycastHit2D hit;
	// Use this for initialization
	void Start () {
        rig = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.A)) {
            rig.velocity = new Vector2(-speed,rig.velocity.y);
        }
        if (Input.GetKey(KeyCode.D))
        {
            rig.velocity = new Vector2(speed, rig.velocity.y);
        }
        //Debug.Log(onGround());
        if (Input.GetKeyDown(KeyCode.Space) && onGround()) {
            rig.velocity = new Vector2(rig.velocity.x,Jump);
        }
    }

    private bool onGround() {
        //Debug.Log(Physics2D.Raycast(transform.position, Vector2.down, 2f,groundLayer));
        return Physics2D.Raycast(transform.position,Vector2.down,1f,groundLayer);
    }
}
