﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    public bool Left,Right;
    public GameObject gb;
    private GameObject current;
    private bool runOnce = true;
    // Update is called once per frame
    private void Start()
    {
        current = GameObject.FindGameObjectsWithTag("Player1")[0];
    }
    void Update () {
        if (runOnce) {
            StartCoroutine(Delay(2));
        }
        if (gb.GetComponent<bounce>().initial == false) {
            //Debug.Log("call");
            gb.transform.position = current.transform.position + new Vector3(0, current.transform.localScale.y / 2f + gb.transform.localScale.y / 2f, 0);
        }
	}

    IEnumerator Delay(float delay)
    {
        runOnce = false;
        yield return new WaitForSeconds(delay);
        Debug.Log("call");
        if (Left == true)
        {
            current = GameObject.FindGameObjectsWithTag("Player2")[0];
        }
        if (Right == true)
        {
            current = GameObject.FindGameObjectsWithTag("Player1")[0];
        }
        if (Left || Right)
        {
            Rigidbody2D rig = GetComponent<Rigidbody2D>();
            rig = gb.GetComponent<Rigidbody2D>();
            //Debug.Log(gb.transform.position);
            rig.velocity = Vector3.zero;
            rig.angularVelocity = 0f;
            gb.GetComponent<bounce>().initial = false;
            Left = false;
            Right = false;
        }
        runOnce = true;
    }

}



