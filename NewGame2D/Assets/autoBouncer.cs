﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class autoBouncer : MonoBehaviour
{


    public float Power;
    public float Angle;
    public bool HitKey = false;
    public bool spicke = false;

    private void OnTriggerEnter2D(Collider2D mycollision)
    {
        if (mycollision.tag == "Ball")
        {
            if (HitKey == false)
            {
                if (gameObject.tag == "Player1")
                {

                    Angle = Random.Range(55, 65);
                    /*
                    if (Angle < 55)
                    {
                        Angle = 55;
                    }
                    else if (Angle > 85)
                    {

                        Angle = 85;
                    }
                    */
                }
                else
                {
                    Angle = Random.Range(130, 150);
                    /*
                    if (Angle < 130) {

                        Angle = 130;
                    } else if (Angle > 175) {

                        Angle = 175;
                    }
                    */
                }
            }
            else
            {
                if (gameObject.tag == "Player1")
                {

                    Angle = 0;
                    /*
                    if (Angle < 55)
                    {
                        Angle = 55;
                    }
                    else if (Angle > 85)
                    {

                        Angle = 85;
                    }
                    */
                }
                else
                {
                    Angle = 180;
                    /*
                    if (Angle < 130) {

                        Angle = 130;
                    } else if (Angle > 175) {

                        Angle = 175;
                    }
                    */
                }


            }
            if (HitKey) {
                mycollision.transform.GetComponent<bounce>().Power = Power * 1.2f;
                HitKey = false;
            } else {
                mycollision.transform.GetComponent<bounce>().Power = Power;
            }
            mycollision.transform.GetComponent<bounce>().Angle = Angle;
            mycollision.transform.GetComponent<bounce>().triger = true;
            //Debug.Log(Angle);
        }
    }

}
